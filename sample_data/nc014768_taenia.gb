LOCUS       NC_014768              13647 bp    DNA     circular INV 17-JUN-2011
DEFINITION  Taenia taeniaeformis mitochondrion, complete genome.
ACCESSION   NC_014768
VERSION     NC_014768.1
DBLINK      Project: 61015
            BioProject: PRJNA61015
KEYWORDS    RefSeq.
SOURCE      mitochondrion Hydatigera taeniaeformis (tapeworm)
  ORGANISM  Hydatigera taeniaeformis
            Eukaryota; Metazoa; Platyhelminthes; Cestoda; Eucestoda;
            Cyclophyllidea; Taeniidae; Hydatigera.
REFERENCE   1  (bases 1 to 13647)
  AUTHORS   Liu,G.H., Lin,R.Q., Li,M.W., Liu,W., Liu,Y., Yuan,Z.G., Song,H.Q.,
            Zhao,G.H., Zhang,K.X. and Zhu,X.Q.
  TITLE     The complete mitochondrial genomes of three cestode species of
            Taenia infecting animals and humans
  JOURNAL   Mol. Biol. Rep. 38 (4), 2249-2256 (2011)
   PUBMED   20922482
REFERENCE   2  (bases 1 to 13647)
  CONSRTM   NCBI Genome Project
  TITLE     Direct Submission
  JOURNAL   Submitted (09-DEC-2010) National Center for Biotechnology
            Information, NIH, Bethesda, MD 20894, USA
REFERENCE   3  (bases 1 to 13647)
  AUTHORS   Liu,G., Lin,R. and Zhu,X.
  TITLE     Direct Submission
  JOURNAL   Submitted (25-DEC-2008) Laboratory of Parasitology, College of
            Veterinary Medicine, South China Agricultural University, 483
            Wushan Street, Tianhe District, Guangzhou, Guangdong 510642, The
            People's Republic of China
COMMENT     REVIEWED REFSEQ: This record has been curated by NCBI staff. The
            reference sequence is identical to FJ597547.
            COMPLETENESS: full length.
FEATURES             Location/Qualifiers
     source          1..13647
                     /organism="Hydatigera taeniaeformis"
                     /organelle="mitochondrion"
                     /mol_type="genomic DNA"
                     /db_xref="taxon:6205"
                     /country="China"
                     /common="tapeworm"
     tRNA            1..65
                     /product="tRNA-Tyr"
     tRNA            69..131
                     /product="tRNA-Leu"
                     /codon_recognized="CUN"
     tRNA            135..196
                     /product="tRNA-Ser"
                     /codon_recognized="UCN"
     tRNA            214..275
                     /product="tRNA-Leu"
                     /codon_recognized="UUR"
     tRNA            281..337
                     /product="tRNA-Arg"
     gene            341..1909
                     /gene="ND5"
                     /db_xref="GeneID:10020623"
     CDS             341..1909
                     /gene="ND5"
                     /codon_start=1
                     /transl_table=9
                     /product="NADH dehydrogenase subunit 5"
                     /protein_id="YP_004062128.1"
                     /db_xref="GeneID:10020623"
                     /translation="MIVLFCFSVMLFALVMFSFLLNYSYVLNLSLLSLSGWNWLVNFI
                     FDKVTICVLMMLLICFLYAYFYTSHYFCNDRAGWDLLQIIVLFVVVMGVLVCTGDFLS
                     TLIFWEYLGVVSFFLILFYDNYLSLRSSAITLVSSRFGDVCLFLIIGLSCFTFNNIIP
                     ALIYLFFVVFSKSGGFPFISWLLEAMRAPTPVSSLVHSSTLVAAGIWFTMRYDLFNFI
                     NDIIWFSNLLLISIFITGVSSMFFLDLKKIVALSTCNNICWCVLYLICGGVVLSLFQL
                     ISHGVSKCVLFMLVGDVMSGSGGSQASNCVYSTRFYGNWNLFSLLIIILGLSGLPFIG
                     VFFTKHYLLSSLIGVVNVFELTFVLICMFLSYFYSFRLCTVITNLKSSNTCGVLFFYE
                     SGLIVCGWLFINFYIFFTMDESIGVGFILSMMLITFQVFSWVVSLLIYDSNLFSSWSS
                     SLFGCDNLVEFCYKIFYTIVFYSSLLFFRWDNLMMCLFGNMGRNSVVNSFNWILLNIL
                     VIGVFSFIFYLVLI"
     tRNA            2324..2388
                     /product="tRNA-Gly"
     gene            2392..3036
                     /gene="COX3"
                     /db_xref="GeneID:10020622"
     CDS             2392..3036
                     /gene="COX3"
                     /codon_start=1
                     /transl_table=9
                     /product="cytochrome c oxidase subunit III"
                     /protein_id="YP_004062129.1"
                     /db_xref="GeneID:10020622"
                     /translation="MSILPLFSASFIGLFLVGLFLWKIWLIFLFFLCVAIMLLIYVLD
                     GLSGKLHYESAFWLFVFSEVMVFGSFLTCCLIFDCWSYDNLSSSLEIPFVGCFVLLGS
                     SITVTAFHHLLGWRYCDFFLLLTIVLGLGFVVLQVAEIEDIGFNLVDSSFYSSSFCTV
                     GLHFSHVLLGVIGLITIFCVGSVSFGVYRCTVLTWYWHFVDYVWLIVYTIVYVC"
     tRNA            3042..3114
                     /product="tRNA-His"
     gene            3137..4183
                     /gene="CYTB"
                     /db_xref="GeneID:10020612"
     CDS             3137..4183
                     /gene="CYTB"
                     /codon_start=1
                     /transl_table=9
                     /product="cytochrome b"
                     /protein_id="YP_004062130.1"
                     /db_xref="GeneID:10020612"
                     /translation="MDLPISFSINYYWSYGFVLSMFMCIQIITGVILSFFYLSNFHIS
                     FSIVTSFSNDSFFTWCLRYFHIIGVNILFILVFIHMGRSLYYSSYNKISVWNVGFLLY
                     LLIMGEAFTGYILPWHQMSYWAATVLTSIVDSLPVVGSIIYKYVVGGFSITGDTLMRV
                     LSVHVCLGFIIIMLMVIHLFYLHKDGSSNPLYNYNGLSDVVYFHSYFTAKDLFLFVSI
                     LTGVLSWLLLSPDLLVDIESYLEADQLNTPVSIKPEWYFLAFYAMLRCIGSKIGGLML
                     VLCFLIFLWVPSSNVSSVYSVTRQIVFWLIISLYVSLTYLGTCHPEYPYLFVCQVFSV
                     SLVVMMLVFKLLNL"
     gene            4188..4448
                     /gene="ND4L"
                     /db_xref="GeneID:10020613"
     CDS             4188..4448
                     /gene="ND4L"
                     /codon_start=1
                     /transl_table=9
                     /product="NADH dehydrogenase subunit 4L"
                     /protein_id="YP_004062131.1"
                     /db_xref="GeneID:10020613"
                     /translation="MISLILIYSSIVCVSFFLTLNRFLNNLIILESLNVLVILFCLLC
                     SSLDNHMIFIAFIVVSTIEVIIGLVVLTQIWECSSLLDLADF"
     gene            4415..5665
                     /gene="ND4"
                     /db_xref="GeneID:10020614"
     CDS             4415..5665
                     /gene="ND4"
                     /codon_start=1
                     /transl_table=9
                     /product="NADH dehydrogenase subunit 4"
                     /protein_id="YP_004062132.1"
                     /db_xref="GeneID:10020614"
                     /translation="MFIFISFSWFLVCLVLLLGLLVFSCGINCFSVINSVVFDNLFVF
                     DSISFYLLILVIILGFYSQVLFINILSCSVRFYLFLSLLFTVFSFCINHSVLFWISYE
                     LSMLPLLFLIFSESPYSERFLAGWYFSGYLISTSLPLILVLLYLSFINDSFFFSDWSM
                     SHNCLNIIYYLISFIFFTKVPLLPFHTWLPIVHAEATSIVSIFLSGYIMKLGLLGVYR
                     CSYFIFDPNFIWYLAFCCLACIGFLVTACTELDGKRWLAFLSLAHIVVPFIGFYISDW
                     SSVNYIFFYCLGHGLSAGIVFGLLWLFYDLCHTRNWILLKSSVNGIGYMISVIFSLLS
                     LCSFPTTIQFFSEVTLVVQSSGLIIYILFWVLYLFFGGLVPLILCGHLLIRSEWYESI
                     GVGFNYLYFLVFLNFWCYLGFLVL"
     tRNA            5666..5729
                     /product="tRNA-Gln"
     tRNA            5728..5789
                     /product="tRNA-Phe"
     tRNA            5787..5850
                     /product="tRNA-Met"
     gene            5861..6379
                     /gene="ATP6"
                     /db_xref="GeneID:10020615"
     CDS             5861..6379
                     /gene="ATP6"
                     /codon_start=1
                     /transl_table=9
                     /product="ATP synthase F0 subunit 6"
                     /protein_id="YP_004062133.1"
                     /db_xref="GeneID:10020615"
                     /translation="MFNFINDFSSLISILYNKIINSDMVYYYLSIILWTMLLFISYRF
                     PYCYSPWLFIVWLIGIVFVCFVSLWLNRLLDNVNTFFASFVPLGTPLYICPLVCIAES
                     ISYIIRPFVLILRPFINISLGCFGAAFIGGAVNSNYLWFLTLILLFFYEVFVALVHWF
                     IVLSILLFSEDH"
     gene            6383..7273
                     /gene="ND2"
                     /db_xref="GeneID:10020616"
     CDS             6383..7273
                     /gene="ND2"
                     /codon_start=1
                     /transl_table=9
                     /product="NADH dehydrogenase subunit 2"
                     /protein_id="YP_004062134.1"
                     /db_xref="GeneID:10020616"
                     /translation="MMFINSRLNFNTVFFFLFFFSGFFCLLCCIVDNLIGFWIFLELC
                     GLSIIPSFFYSNKVGLYNFYSCVLSYIIVSGLSSVLVVLGLMFLGLYYFVYFGFALKF
                     GLFPFLLWIYRVFSVGNWLFIFFLSVLLKFPVLFFCFLYQNVNVNLVYVDCFLTILLC
                     SILIWFFSLSWEYVWCHISLSSIATLVVACFCSSVEICFFIYLYYFFWATLTIIYLNV
                     ISDMVDLKSFMFWGFCFLLLITPISVPLIYKISVCIGILYSSIYLLLIWSVYSFSEQF
                     FLYKLASGYFFSGVYNNWLN"
     tRNA            7275..7338
                     /product="tRNA-Val"
     tRNA            7347..7412
                     /product="tRNA-Ala"
     tRNA            7418..7483
                     /product="tRNA-Asp"
     gene            7490..8383
                     /gene="ND1"
                     /db_xref="GeneID:10020618"
     CDS             7490..8383
                     /gene="ND1"
                     /codon_start=1
                     /transl_table=9
                     /product="NADH dehydrogenase subunit 1"
                     /protein_id="YP_004062135.1"
                     /db_xref="GeneID:10020618"
                     /translation="MIIFTFISGLFGLLISLLVIAFFILGERKILGYSQYRKGPNKVG
                     FLGLFQSFADLLKLIVKFKWFYFQSRNFVGLVGVMLLVLLSIFYCFVYGDYYGSKIGG
                     FSMLWFLVITSLCGYAVLCAGWGSFNNYSFLSCIRCSFSSISFEACFMSIIIFSGLCY
                     NSYNLSDLINVDWASLFIFPCAYIIFLIGILCETNRTPFDYGEAESELVSGFNVEYSG
                     VYFTCLFACEYIIIFIFSWIGVIIFLGDSIFSMILLFINLLFFMWARATLPRVRYDFF
                     VNFFWEVGLLMVIFSLFCIVN"
     tRNA            8384..8451
                     /product="tRNA-Asn"
     tRNA            8459..8520
                     /product="tRNA-Pro"
     tRNA            8520..8581
                     /product="tRNA-Ile"
     tRNA            8585..8645
                     /product="tRNA-Lys"
     gene            8650..8989
                     /gene="ND3"
                     /db_xref="GeneID:10020617"
     CDS             8650..8989
                     /gene="ND3"
                     /note="TAA stop codon is completed by the addition of 3' A
                     residues to the mRNA"
                     /codon_start=1
                     /transl_except=(pos:8989,aa:TERM)
                     /transl_table=9
                     /product="NADH dehydrogenase subunit 3"
                     /protein_id="YP_004062136.1"
                     /db_xref="GeneID:10020617"
                     /translation="MLLAGFSLFILLSVIIGFFCCSLLNKVVMFEGGWPSCYECGFFN
                     GLMNLNCFSFTYFDLLVVFVIFDLEISLLLNMPTQGLMYWSFVGYYTFLCILLFGFLI
                     EIFFGYVKWVY"
     tRNA            8990..9048
                     /product="tRNA-Ser"
                     /codon_recognized="AGN"
     tRNA            9050..9111
                     /product="tRNA-Trp"
     gene            9112..10723
                     /gene="COX1"
                     /db_xref="GeneID:10020619"
     CDS             9112..10723
                     /gene="COX1"
                     /note="TAA stop codon is completed by the addition of 3' A
                     residues to the mRNA"
                     /codon_start=1
                     /transl_except=(pos:10723,aa:TERM)
                     /transl_table=9
                     /product="cytochrome c oxidase subunit I"
                     /protein_id="YP_004062137.1"
                     /db_xref="GeneID:10020619"
                     /translation="MISLNWLLSWIFTLDHKRVGIIYTLLGLWSGFVGLSFSLLIRVN
                     FLEPYYNVIPLDCYNFLVTNHGIIMIFFFLMPILIGGFGNYLLPLLGGLADLNLPRLN
                     ALSAWLLVPSLAFLVVSMYLGAGIGWTFYPPLSSSLFSEGVGVDFLMFSLHLAGASSL
                     FGSINFICTLYSIFMTNIFSRTSIVLWSYLFTSILLLVTLPVLAAAITMLLFDRNFSS
                     AFFDPLGGGDPVLFQHMFWFFGHPEVYVLILPGFGIISHICLSISMSSDVFGFYGLLF
                     AMFSIVCLGSSVWGHHMFTVGLDVKTAVFFSSITMIIGVPTGIKVFTWLYMLLNARVN
                     KSDPVLWWIVSFIILFTFGGVTGIVLSACVLDNVLHDTWFVVAHFHYVMSLGSYISII
                     IMFIWWWPLITGLSLNKCLLQCQCIVSQIGFNLCFFPMHYFGLCGLPRRVCIYECAYN
                     WINIVCTVGSFISAFSGCFFVFILWESIVSSNYVLGSYGISGCMADFFVSPMACHNDY
                     YCYPYSVDYTYGVYYMRWIDDCTYVFARG"
     tRNA            10728..10797
                     /product="tRNA-Thr"
     rRNA            10797..11755
                     /product="l-rRNA"
                     /note="16S ribosomal RNA"
     tRNA            11756..11815
                     /product="tRNA-Cys"
     rRNA            11816..12532
                     /product="s-rRNA"
                     /note="12S ribosomal RNA"
     gene            12542..13123
                     /gene="COX2"
                     /db_xref="GeneID:10020620"
     CDS             12542..13123
                     /gene="COX2"
                     /codon_start=1
                     /transl_table=9
                     /product="cytochrome c oxidase subunit II"
                     /protein_id="YP_004062138.1"
                     /db_xref="GeneID:10020620"
                     /translation="MNLSLLYYDIVCYIIAVCMFIICFVYIMLGWNLFMSKGSVNFGG
                     ENQFVELSWTIVPTMVVLVLCALNVNFITSDLDCFSSETIKIIGHQWYWTYEYPDGYY
                     DSFMTKDCFLVDKPLRMFYGVPYHLVVTSADVIHSFSVPSLNLKMDAIPGRLNHMFFC
                     PSQHGSFIGYCAELCGVNHGVMPIVIEVIDTMD"
     tRNA            13124..13196
                     /product="tRNA-Glu"
     gene            13195..13642
                     /gene="ND6"
                     /db_xref="GeneID:10020621"
     CDS             13195..13642
                     /gene="ND6"
                     /note="TAA stop codon is completed by the addition of 3' A
                     residues to the mRNA"
                     /codon_start=1
                     /transl_except=(pos:13642,aa:TERM)
                     /transl_table=9
                     /product="NADH dehydrogenase subunit 6"
                     /protein_id="YP_004062139.1"
                     /db_xref="GeneID:10020621"
                     /translation="MVSILLSLYLFNLFLFSLISSCIYYCIILIINALLSSYIVYEVM
                     GFSWYSLIFCLIYVGGVYILFIFVSFFNPNDSVMISWGFNLVSLVLIFFVFGVSIYMF
                     CELIEFEFSSCVCNFNEIWLYVCFGLMLMFGFVVLSMLMSYGVGFYR"
ORIGIN      
        1 tctagcttaa catatttaat gtggagggtt gtaaactctt ttaaggttta taccagttag
       61 gattatatgt aaaggtgcca gaattaatgg gattaattta ggattaattt atgatttttt
      121 atctctttat taatacgcat aaaaactcat gtttgatttg aaatcaaatt tattgttttt
      181 aataacaata tgagttccta tatttattta tgtgtagtta tgtcagaatt atatgagtta
      241 gttttaagca ttaattatgg aatttcctaa ctactaatct aacatataga agacttatgt
      301 tacggccata agaatggtaa tgttataccg tatgtttatt atgatagttt tattttgttt
      361 tagtgtaatg ctttttgctt tggtaatgtt taggttttta ttaaattata gttatgtact
      421 aaaacttaga ttattaaggt taagagggtg gaaatgatta gtcaaattta tatttgataa
      481 ggtgactatt tgtgttttaa tgatgttgct tatatgcttc ttgtatgctt atttttatac
      541 tagtcattat ttttgtaatg atcgtgctgg ttgagatttg ttacaaatta ttgttttatt
      601 tgttgtagta atgggtgtat tggtatgtac tggtgatttt ttaagtactc tgattttttg
      661 agaatatctt ggggttgtta gatttttttt gattttattt tatgataaat atttaagttt
      721 gcgttcatcg gctattactt tagtttcttc tcgttttggg gatgtttgcc tgtttttgat
      781 aattgggtta agatgtttta cttttaataa tattattcct gctttaattt atctattttt
      841 tgttgtgttt tctaagagtg gaggatttcc ttttataaga tggttgttag aagctatgcg
      901 tgctcctacc ccagttagat ctttagttca ttcttctaca ttggtagctg ctgggatttg
      961 atttactatg cgttatgatt tatttaattt tattaaagat ataatttgat tttctaattt
     1021 gttattaatt agtatattta taactggtgt aagaagaatg ttttttttag atttaaagaa
     1081 gattgtggca ttgtccactt gtaataatat ttgttgatgt gtattatatt taatttgtgg
     1141 gggagttgtg ttgtcattgt ttcaacttat tagtcatgga gtgtctaagt gtgtattgtt
     1201 tatgttagtt ggggatgtaa tgagaggaag aggtggatct caggctagta aatgtgtgta
     1261 tagaactcga ttttatggta attgaaactt gtttagatta ttgataataa ttttaggtct
     1321 ttcaggatta ccttttatag gtgtgttttt tactaagcat tatttattat caagacttat
     1381 tggtgttgtt aatgtttttg aattgacgtt tgttttgatt tgtatgtttt tatcttactt
     1441 ttattcattc cgtttatgta ctgtaattac taatttaaag tctagtaata catgtggtgt
     1501 attatttttt tatgaatctg gtctgatagt atgtgggtgg ttgttcataa aattttatat
     1561 attttttact atggatgaaa gcataggtgt tgggtttata ttaagcatga tgttaataac
     1621 atttcaagtg ttttcttgag tggtaagttt actaatatat gatagaaaat tatttagatc
     1681 atgaaggaga agattgttcg gttgtgataa tttagtggaa ttttgttata agatttttta
     1741 caccattgtt ttttattcta gtctgttgtt ttttcgttga gataatttaa tgatgtgttt
     1801 atttggtaat atgggccgaa aaagggttgt aaataggttt aaatgaattt tattaaatat
     1861 attggttatt ggggttttta gctttatatt ttatttggtg cttatttaat ttgatatggt
     1921 aaaattttag gttcgtcttg cgtcataaag ttttgttcat caataaataa tatttattta
     1981 ttataataac aaatataaat ataaaaaata gatgcaatat atacaacata tataagataa
     2041 gatatatagg gggtcacccc ctatatatct tatcttatat atgttgtata tataatatat
     2101 agatatatct ctctattcat agagagatat atctatatat taaataaact ataagtcatc
     2161 aattatttac tatagttaat agttatttaa tttgtatatt gatagagttg tgaagactta
     2221 gtggttgtct gatgacttat agtttattta atttgatatg gtaaaatttt aggttcgtct
     2281 tgcgtcataa agttttgttc atcaataaat aatattgtgt tctatgctat tagtataaat
     2341 tcattatttt tcttttccac agaaaagatc taattattag atggcataat tatgtctata
     2401 cttcctttat ttagtgcctc ttttataggt ttatttttgg taggtttatt tttatgaaag
     2461 atttgattaa tatttttatt ttttttgtgt gttgctataa tgttgttaat ttatgtgttg
     2521 gatggtttat ctggtaagct tcattatgaa tctgcttttt ggttgtttgt gtttagtgaa
     2581 gttatggttt ttggtagatt tttgacttgt tgtttaattt ttgattgttg atcatatgat
     2641 aatttatcta gttctttgga gataccattt gttgggtgtt ttgtgcttct tggttctagt
     2701 ataacggtta ctgcatttca tcatttgtta ggttggcgat attgtgattt ttttttactt
     2761 ttaactattg tgttagggtt ggggtttgtt gttttacaag ttgctgagat cgaggatata
     2821 ggatttaatt tagttgattc tagattttac tcaagaagct tttgtacagt tggtttacat
     2881 tttagtcatg ttttattagg agtgataggt ttgattacta tattttgtgt tggtagcgtt
     2941 agctttgggg tttatcgctg tactgtttta acttgatatt ggcattttgt agattatgtt
     3001 tgattaatag tttatactat tgtatatgtt tgttaatagt ttactaatag gttatgaaat
     3061 aaactaatag attgtggttc tgttgtatac ttttagtatt agtaaattta tgtttaattt
     3121 attgcgttgt aatttgatgg atttacctat tagtttctct ataaattatt attgaagtta
     3181 tggttttgtt ctttctatgt ttatgtgtat tcaaattatt actggtgtaa tattatcctt
     3241 tttttacctt agtaattttc atattagatt ttcaattgtt actagatttt ctaaagattc
     3301 tttttttact tgatgcttac gatactttca tataattggg gttaatatat tgtttatctt
     3361 agtttttatc cacatgggtc gttctttata ttattctaga tataaaaaga ttagtgtttg
     3421 gaaagttgga tttcttttat atttgttaat aatgggtgaa gcttttaccg gatatatttt
     3481 gccatgacat cagatgtctt attgagcagc tactgtgttg acttcgatag ttgatagttt
     3541 gccagttgtg ggtagtatta tttataagta cgttgtgggt ggtttttcta ttactggtga
     3601 tacattaatg cgtgtattat ctgtacatgt gtgtttaggt tttattataa taatgttaat
     3661 ggtcattcat ttgttttatc ttcataagga tggtagaagt aatcccttat ataattataa
     3721 aggtttatct gatgtagttt attttcattc atattttaca gcgaaggatt tatttttatt
     3781 tgtgagtatt ctaactggag tattaagttg attgctatta agtccagatt tgttagttga
     3841 tatagaatct taccttgaag ctgatcaact taatactcca gttagaataa agccagaatg
     3901 atacttttta gctttttatg ctatgttacg ttgtattggg tcaaagatag gtgggttaat
     3961 gttagttttg tgttttttaa tatttttatg ggttcctaga agaaaagtgt ctagtgtgta
     4021 tagagttaca cgtcaaattg ttttttggtt gataattagt ttatatgttt ctttaactta
     4081 tttaggtact tgtcatccag aatatccata tttgtttgtt tgtcaggtgt ttagagtgtc
     4141 attagttgta atgatgcttg tgtttaagtt attaaattta taataaaatg attagattaa
     4201 ttttgattta tagaagtata gtatgtgtta gatttttttt aacgttaaat cggttcttaa
     4261 ataaattaat tatattagaa agattgaaag tattggttat attgttctgt ttattatgtt
     4321 ctaggttaga caatcatatg atatttatag cttttatagt agtttctaca atagaggtta
     4381 taattggttt agtagtgtta actcaaattt gagagtgttc atctttatta gatttagctg
     4441 atttttagtt tgtttagtgt tactgcttgg tttattagta ttttcatgcg gtataaattg
     4501 ttttagagtt attaatagag tagtttttga taatttgttt gtgtttgatt ctatatcatt
     4561 ctatttatta attctggtaa ttattttagg cttttattct caagtgttat tcattaatat
     4621 attatcttgc agtgttcgtt tctatctatt tttaagattg ttgtttacag tttttaggtt
     4681 ttgtattaat catagagtat tattttgaat aagatatgaa ttatcaatgc ttccactgtt
     4741 atttttaata tttagtgagt ctccttattc agaacgtttt ttagcagggt ggtattttag
     4801 tggttatctt ataagtacaa gtttaccttt gattttagta ttgttatact tatcctttat
     4861 aaatgattca ttttttttta gagattgaag catgtctcat aactgtttaa aaattattta
     4921 ttatttaata tcttttatat tttttaccaa ggttcctttg ctcccatttc atacatgatt
     4981 acctatagta catgctgaag ctactagtat agtatcaatt tttttgagtg gatatatcat
     5041 gaagttgggt ttattaggag tatatcgttg ttcatatttt atatttgatc ctaattttat
     5101 atgatatctt gctttttgct gtttggcttg tataggtttt ttagtaacag catgtacaga
     5161 attagatgga aagcggtgat tagcattttt aagtttagcc catattgttg ttccttttat
     5221 tggtttctat atcagtgatt gaaggtcagt taattatata tttttttact gtttggggca
     5281 tggtctaagt gcaggtattg tatttgggtt attatgatta ttttatgatc tatgtcatac
     5341 tcgaaaatga attttgttga agtctagcgt aaatgggatt ggttatatga taagagtcat
     5401 ttttaggtta ttaaggttat gttcatttcc tacaactatt caattttttt ctgaagtgac
     5461 tttggttgta caaaggtctg gtttaattat ttatatatta ttttgggttt tgtatttatt
     5521 ttttggtggg ttggttcctt taatattatg cggccattta ttaattcgta gagagtgata
     5581 tgaaagcata ggtgttgggt ttaaatattt atatttttta gtatttttaa atttctggtg
     5641 ttatttaggt tttttagtat tatagtttaa tgaggtgttt atgtgcattt tacattttgg
     5701 ttgtgaaggt gattagtaat ccgttaattt ctcttagctt aagttaaagt atcaatttga
     5761 agagttggag ataattatat tagagagatt gataagttaa aaaaactgtg gggttcatgt
     5821 ctccttaata catatttata tgttcaatcg agtatattta atgtttaatt tcataaatga
     5881 ttttagctct ttaattagaa ttttgtataa caagataata aaaagtgata tggtttacta
     5941 ttatttaagc ataattctgt gaactatgtt attatttata agttatcgtt ttccctactg
     6001 ttatagccct tgattgttta ttgtatgatt aatagggatt gtttttgtat gttttgttag
     6061 attatggttg aatcgattat tagataatgt taatactttt tttgctagat ttgtgccatt
     6121 gggaacgcct ctttatattt gtcctttagt gtgtatagct gagtctataa gttatattat
     6181 acgtcctttt gtattgatac tgcggccatt tattaatata agtttaggtt gttttggtgc
     6241 tgcttttatt ggaggggcag taaatagtaa ttatttgtga tttttaacat tgatattatt
     6301 atttttttat gaagtttttg tggctttagt acattgattt attgttctta gtatattgtt
     6361 attttctgaa gatcattagt gtatgatgtt tataaatagt cgattaaatt ttaatactgt
     6421 attttttttt cttttttttt tttctggctt tttttgtttg ttgtgttgta ttgttgataa
     6481 attaataggt ttttgaatat ttttagaatt atgtggttta tcaattatac cgtcattttt
     6541 ttatagtaaa aaggttggtt tgtataaatt ttataggtgt gttttgaggt acattatagt
     6601 gtctggtcta tcttcagttt tagtagtatt ggggttaatg tttttgggtt tgtattattt
     6661 tgtttatttt gggtttgcgt taaagtttgg tttatttcca tttttgttgt ggatatatcg
     6721 agtatttagt gttggaaatt ggttatttat ttttttttta agggttttat taaagtttcc
     6781 agttttattt ttttgttttt tataccaaaa tgttaatgtt aaattggttt atgttgattg
     6841 ttttttgact attcttcttt gttctattct tatttgattt tttagtttat cttgagagta
     6901 tgtttggtgc catatttctt tgtcttctat tgctactttg gtggtagcat gtttttgtag
     6961 tagagttgaa atttgttttt ttatatattt gtactatttt ttttgggcta cacttaccat
     7021 aatttatttg aaagttattt ctgatatggt tgatttaaag agttttatgt tttgagggtt
     7081 ttgcttttta ttattaataa ctccaatttc tgtaccttta atctataaga ttagagtgtg
     7141 tatcggtatt ttatactctt caatatattt gttgttaata tgaagggttt atagtttttc
     7201 tgaacagttt ttcttatata agctagctag tggttatttt tttagtggtg tttataaaaa
     7261 ttggttaaat tagttgtaaa gtagtttata aaaatgtctg ttttacacac aggagaactc
     7321 gtttgagctt tactatggtt tagtttaaca aaatagttta atgaaaaaat attgggtttg
     7381 cgtctcaaag atggattgtg gttctgttgt tatatttgta attttagttt aagaataaaa
     7441 tagtgatttg tctagtcata gatgatataa atatcaagtt actttggtaa tgattatttt
     7501 tacttttata tctggtttat ttggattatt gataagttta ttagtgatag ctttttttat
     7561 attaggtgaa cgtaagatat tagggtattc tcaatatcgt aagggcccta ataaggttgg
     7621 ttttttagga ttgtttcaga gttttgctga tttgttgaag ttaattgtga agtttaagtg
     7681 gttttatttt caaagtcgta attttgttgg tttagttggt gtaatgttgt tagttttatt
     7741 atctatattt tattgttttg tttatggtga ttattatggg tcaaagatag gtgggttttc
     7801 aatgctttgg tttttggtta ttactagttt atgtggttat gctgtgttat gtgccggttg
     7861 gggtagtttt aataattatt catttttaag ttgtatccgt tgtagattta ggtctataag
     7921 gtttgaagcg tgttttatga gtataattat ttttaggggt ttatgttata aaagatataa
     7981 attaagggat ttaataaatg ttgattgagc ttctttattt atatttcctt gtgcttatat
     8041 aatattttta ataggtatat tatgtgaaac taatcgtact ccgtttgatt atggtgaagc
     8101 tgaaagagaa ttggttagcg gatttaatgt tgaatatagt ggtgtatatt ttacttgttt
     8161 atttgcttgt gagtatataa ttatatttat tttttcttga ataggggtga ttatattttt
     8221 aggggatagt atatttagca tgattttatt gtttataaat ttgttatttt ttatgtgggc
     8281 gcgtgctaca ttaccacgtg tacgttatga tttttttgtt aaattttttt gagaagtagg
     8341 tctgttaatg gttatattta gcttattttg tatagtaaat taagtctata tagattaaat
     8401 gtaaatcgtg atgctgttaa cttcaagaaa tagttgttac tattatagtc gttacattct
     8461 tatcttagtt taaaaagaat gatggttttg gggacctttg gtctcagtga gagatttggt
     8521 taatagggct gcgaagcagg ttactttgat atagtaaata gtgaattatt attccgttaa
     8581 tatgactcat atatcttatt aaaagtacta ggttcttacc ctagagatgt attatactat
     8641 gggtggagta tgttattggc tggtttttct ttatttattt tattaagtgt tattattggc
     8701 tttttttgtt gcagattatt aaaaaaggtg gttatgtttg aagggggatg gcctagatgt
     8761 tatgaatgtg ggttttttaa aggtttgatg aaacttaaat gttttagatt tacttacttt
     8821 gatttgttag tggtttttgt tatttttgat ctagaaatat ctttgttgtt aaatatgcct
     8881 actcaaggat taatgtattg gagttttgta gggtattata catttttatg tatattgtta
     8941 tttgggtttt taattgagat tttttttggt tatgttaagt gagtttatta gagaaatgta
     9001 tgaagttact gctaataatt ttttgtcaat ttattttggc tttctcttta taagattaag
     9061 ttaatatgga ctgtatgttt tcaaaacatt tagaggtttt accatcttat gatgataagt
     9121 ttaaactgac tattaagttg aatatttacg ttggatcata agcgtgttgg tattatttat
     9181 actttgttag gattgtgatc tggttttgta ggtttaaggt ttagtttatt aattcgggta
     9241 aaatttttag aaccttatta taaagttata ccattagatt gttataattt tttggttact
     9301 aatcatggta taattatgat ttttttcttt ttgatgccaa ttctaattgg aggatttggt
     9361 aattatttat tgccattatt aggagggtta gcagatttaa atttacctcg attaaatgca
     9421 ttaagtgctt gattattggt tccttcatta gcttttttag tagttagtat gtatttagga
     9481 gcgggtattg ggtgaacatt ttatcctcct ttatcatcat ctttattttc agagggtgtt
     9541 ggtgtagatt ttttaatgtt ttcgttacat ttggctggtg cttctagcct atttggttct
     9601 ataaaattta tttgtacttt gtatagaatt tttatgacta atattttttc tcgtacatct
     9661 attgtacttt gatcttattt gtttacttct attttactat tggtaacttt accagtttta
     9721 gcagctgcta ttactatgtt gctatttgat cgtaaattta gttctgcttt ttttgatcct
     9781 ttaggtggtg gtgatccagt tttatttcag catatgtttt gattttttgg tcatcctgag
     9841 gtgtatgtat taattcttcc tggatttggt attattagac atatatgctt aagaattagt
     9901 atgtcttcgg atgtgtttgg tttttatggt ttattgtttg ctatgttttc tatagtttgt
     9961 ttaggaagaa gggtgtgagg tcatcatatg tttactgttg ggttagatgt aaagacggct
    10021 gtgtttttta gctctataac tatgattatt ggagtaccta caggtataaa ggtttttaca
    10081 tgattgtata tgttgttgaa tgctcgagtc aaaaagagtg atcctgtttt atgatgaatt
    10141 gtttctttta ttattctgtt tacgtttggt ggggttactg gtatagtatt atcggcttgt
    10201 gttttagata aagtgttaca tgatacttga tttgtagtag ctcattttca ttatgttatg
    10261 tcgcttggct cttatataag aataataata atgtttattt gatgatgacc attaataact
    10321 ggtttaaggt tgaataagtg tttattgcag tgtcagtgta tagtttctca aattgggttt
    10381 aatttgtgtt tttttcctat gcattatttt ggattatgtg ggttaccacg tcgtgtttgt
    10441 atttatgaat gtgcttataa ttgaataaat atagtatgta ctgttggatc ttttatatcg
    10501 gcttttagag ggtgtttttt tgtatttatt ttatgggaat caatagtgag ttctaattat
    10561 gttttaggtt catatggtat ttctgggtgt atggctgatt tttttgtgag accaatggct
    10621 tgtcataaag attattattg ttatccttat agagttgatt acacatatgg tgtttattat
    10681 atgcgttgaa ttgatgattg tacatatgtt tttgctcgtg ggtgtgttga tggttattta
    10741 gtttaataaa aatatgggtt ttgtagtcct aagatgattt taatcattaa cctaatttta
    10801 gtaaatactt atatatgtgt ttaggtttat ttgccttttg catcatgctt attggattgt
    10861 tttaaaatat tttgttaacc aaaaaatcta gatcttataa ttatttgttt tgaacttaaa
    10921 aatcttaagt aaagaataat gaaaataatt ttatggtgtg ataaattatt cgttagattt
    10981 tatatttgtt ttactgcttt ctatttaata atatgtaatt ggctattagg ttaattataa
    11041 gtttatatta ttttttctgt tgtgttttta ttgggttaaa agtacccatt atttgtaaag
    11101 ttgttataaa cttggatttg tgttaattta attaagtatt tagcagtata taaataaaat
    11161 ttaggtggta ataatcaata aataagtaat ccaattatat taattatttc tagtaacttt
    11221 tccgtctgtt tattaaaaac attgccaatt taaattttag ttggtagtac ctgcccagtg
    11281 ttgttaataa atggccgcag tatattgact gtgcaaaggt agcataatta attgcctttt
    11341 aattgggggc ttgtttgaat ggtttgacga gataagtttt aatatttaat atttgtgtga
    11401 aattaatgtt aagggttcag aatccttata tattaataag acggaaagac cctaggatct
    11461 tttgtttttt gtttggggca aatttgtata gtttatagaa attgtgaccc taatgggtta
    11521 ttgggttaag ttaccctagg gataacagag taataattaa tgagagatca tatcgaatta
    11581 attgtttgct acctcgatgt tgacttaggt ttaaggctta ggtgtagtag tttaagttgt
    11641 tggtctgttc gaccttaaaa acctccatga gttgagttaa gaccggcgtg agccaggtcg
    11701 gttcttatct attgtagaaa tttatcagta cgaaaggata gtaaattttt ttattgagta
    11761 tgatataaaa ttggttttgc aaaaactaat tagaattagg ttaattccat actttaattg
    11821 tttaattgtg gcaatagaaa gtttattctt tattatctgc ataaatagtt attagatatt
    11881 atgatgagat tgttaagttt tgattcatta gcagttagtt ttttgttaaa atttgtgttt
    11941 taaaagaata aagaatataa aggggatagg acacagtgcc agcatctgcg gttaatctgt
    12001 tttctttgct tttaaataga ggtgattaaa ttaataataa gtaaaaatgg gttaaatttg
    12061 tttattgtgt ttttaagtga tttatcttat ataaaatagg ttttatgaca gggattagat
    12121 accccattaa tactattggt aatatatctt agttttataa ctaaaataat ttggcagtga
    12181 gcgattctta ttaggggaag gtgtggtgta aaggatggtc cgcctaataa ttcactttta
    12241 ttatattggt gtatatctgg tttaatatta ttgttgagta acagaagttg tgtaatatta
    12301 tttaagccaa gtctatgtgc tgtttataaa agtattcatg cgttacatta ataaatgtta
    12361 gattgtgata tttgataatt taggacttaa aagtaatgtt taataagtta gtaaacgtga
    12421 aaagagttta gctcaggtac acaccgcccg tcaccctcga tgattattga ggtaagtcgt
    12481 aacaaggtaa ctttaaatga atttgaagtt agttactggt taataaattt aatcagttaa
    12541 aatgaaacta tcattgttgt attatgatat tgtatgttat ataatagctg tgtgtatgtt
    12601 tataatttgt tttgtttata taatgctagg ttggaatttg tttatgagta agggtagtgt
    12661 taaatttggt ggagaaaatc aatttgttga gctttcatga acaattgtac ctacaatggt
    12721 cgttttagtt ttgtgtgcct tgaatgttaa atttataact agtgatttag attgtttttc
    12781 tagagaaact attaagatta taggtcatca gtgatattgg acttatgaat atccagatgg
    12841 ttattacgat tcttttatga ctaaggattg ttttttggtt gataagccat tgcgtatgtt
    12901 ttatggcgtc ccgtatcatt tagttgttac ttcagctgat gttattcatt cattttctgt
    12961 accatcttta aatttaaaga tggatgctat accaggacgt ttaaatcata tgtttttttg
    13021 tccttctcaa catggttctt ttataggtta ctgtgctgag ttatgtgggg tgaatcatgg
    13081 cgttatgcct atagttatag aagtaataga tacaatggac tagttttgtt ttagtataag
    13141 tgttgattat tttaactttt cgtgttaagg atagttttta actaaacaaa agtagtggtt
    13201 agtattttat taagtttata tttatttaat ttatttctgt tttctttaat tagaagttgt
    13261 atatattatt gtataatttt gattattaaa gctttgttgt caagctatat tgtgtatgaa
    13321 gtaatgggat ttagttgata ttctttaatt ttttgtctta tatatgtagg tggtgtgtac
    13381 atattattta tttttgtttc tttttttaac cctaatgata gagtcatgat tagttgaggg
    13441 tttaatttag tgagattagt gttgattttt tttgtttttg gtgtgtctat atatatgttt
    13501 tgtgagttga ttgaatttga atttagaaga tgtgtgtgta aatttaaaga gatttgattg
    13561 tatgtatgct tcggtttgat gttgatgttt gggtttgttg ttttaagaat gttgatgtct
    13621 tatggagtcg ggttttatcg ttaaatt
//

