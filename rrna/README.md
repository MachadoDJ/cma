# Source of data

Sequences from EMBL-EBI's EBA and NCBI's Genbank, downloaded on Jan. 10, 2019.

## GenBank's search terms

1. `(((mitochondrial AND rRNA) AND (Cestoda OR Eucestoda)) AND ("small subunit"
		OR "12S" OR "s-rRNA")) NOT ("partial" OR "scaffold" OR "complete genome" OR
		"CDS" OR "large subunit" OR "16S" OR "l-rRNA" OR "tRNA" OR "28S" OR "18S" OR "similar")`
2. `(((mitochondrial AND rRNA) AND (Cestoda OR Eucestoda)) AND ("large
		subunit" OR "16S" OR "l-rRNA")) NOT ("scaffold" OR "complete genome" OR
		"CDS" OR "small subunit" OR "12S" OR "s-rRNA" OR "tRNA" OR "28S" OR "18S" OR
		"similar")`

Also, we downloaded 251 complete mitogenomes of Cestoda from NCBI's GenBank to
extract their rRNA sequences (used).

## EMBI's ENA search terms

1. 16S rRNA mitochondrial Cestoda
2. 12S rRNA mitochondrial Cestoda

# Auxiliary scripts

This direcotry includes: `prepare_hmm_database.sh` and `filterfadb.py` (to prepare the
databases only). See usage details inside `prepare_hmm_database.sh`.

## Example

```bash
bash prepare_hmm_database.sh lrrna.fasta 100 5000 0.1
bash prepare_hmm_database.sh srrna.fasta 100 5000 0.1
```

# Note

Data herein was combined with data from complete mitogenomes from NCBI.