#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# filterfadb.py
# Reads entries from a multifasta file and removes sequences based on size and quality.

# Import modules and libraries
import argparse, sys
from Bio import SeqIO

# Set arguments
parser=argparse.ArgumentParser()
parser.add_argument("-i", "--input", help="File with multiple sequences in FASTA file.", type=str, required=True)
parser.add_argument("-o", "--output", help="Output file's name (default = output.fasta)", type=str, required=False)
parser.add_argument("--min", help="Minimum sequence length (default = 100).", type=int , default=100, required=False)
parser.add_argument("--max", help="Maximum sequence length (default = 5.000).", type=int, default=5000, required=False)
parser.add_argument("-q", "--quality", help="Maximum percentage (0 to 1) of ambiguous characters (default = 0.1)", default=0.100, type=float, required=False)
parser.add_argument("--stats", help="Print basic statistics and quit.", action="store_true",default=False, required=False)

args=parser.parse_args()

# Define functions
def get_stats():
	length = []
	ambiguities = []
	gc = []
	sys.stderr.write("Reading records from {}\n".format(args.input))
	for record in SeqIO.parse(args.input, "fasta"):
		sys.stderr.write("-- Sequence ID = {}\n".format(record.id))
		seq = record.seq.upper()
		total = len(seq)
		length += [total]
		ambiguities += [sum([1 for bp in seq if not bp in ["A", "C", "T", "G", "U"]]) / total]
		gc += [sum([1 for bp in seq if bp in ["C", "G"]]) / total]
	sys.stdout.write("Avg. length = {} ({} to {})\n".format( sum(length)/len(length), min(length), max(length) ))
	sys.stdout.write("Avg. ambiguity content = {} ({} to {})\n".format( sum(ambiguities)/len(ambiguities), min(ambiguities), max(ambiguities) ))
	sys.stdout.write("Avg. GC content = {} ({} to {})\n".format( sum(gc)/len(gc), min(gc), max(gc) ))
	return

def filter():
	handle = open(args.output, "w")
	length = []
	ambiguities = []
	gc = []
	sys.stderr.write("Reading records from {}\n".format(args.input))
	for record in SeqIO.parse(args.input, "fasta"):
		seq = record.seq
		total = len(seq)
		if total < args.min or total > args.max:
			sys.stderr.write("-- Sequence ID = {} (rejected, length = {})\n".format(record.id, total))
			pass
		else:
			ambiguity = sum([1 for bp in seq if not bp in ["A", "C", "T", "G", "U"]]) / total
			if ambiguity > args.quality:
				sys.stderr.write("-- Sequence ID = {} (rejected, ambiguities = {})\n".format(record.id, ambiguity))
				pass
			else:
				sys.stderr.write("-- Sequence ID = {} (accepted)\n".format(record.id))
				length += [total]
				ambiguities += [ambiguity]
				gc += [sum([1 for bp in seq if bp in ["C", "G"]]) / total]
				handle.write(">{}\n{}\n".format(record.id, record.seq))
	handle.close()
	sys.stdout.write("No. of sequences = {}\n".format(len(length)))
	sys.stdout.write("Avg. length = {} ({} to {})\n".format( sum(length)/len(length), min(length), max(length) ))
	sys.stdout.write("Avg. ambiguity content = {} ({} to {})\n".format( sum(ambiguities)/len(ambiguities), min(ambiguities), max(ambiguities) ))
	sys.stdout.write("Avg. GC content = {} ({} to {})\n".format( sum(gc)/len(gc), min(gc), max(gc) ))
	return



# Execute functions
if args.stats:
	get_stats()
	exit()
else:
	filter()

# Quit
exit()
