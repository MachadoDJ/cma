#!/bin/bash
set -o errexit

# prepare_hmm_database.sh
# usage: bash prepare_hmm_database.sh <fasta> <min. length> <max. length> <max. ambiguity>
# Run `prepare_hmm_database.sh clean` before executing this.

IN=$1
MIN=$2
MAX=$3
AMB=$4

function clean { # Clears the path to build new databases
	rm *.align *.hmm *.filtered
	wait
}

function filter { # Filter sequences in FASTA format based on their length and amount of ambiguities
	echo "==> Filtering sequences using filterfadb [`date`] <=="
	python3 filterfadb.py --input ${IN} --output ${IN%.*}.filtered --min ${MIN} --max ${MAX} --quality ${AMB}
	wait
}

function mafft_align { # Alignment
	echo "==> Aligning sequences with mafft [`date`] <=="
	mafft --auto --thread -1 ${IN%.*}.filtered > ${IN%.*}.align
	wait
}

function build_hmm { # Build draft hmm databese
	echo "==> Building hmm database with hmmer [`date`] <=="
	hmmbuild ${IN%.*}.hmm ${IN%.*}.align
	wait
}

function calibrate { # Calibrate HMM database for better results
	echo "==> Calibratins hmm database with hmmer [`date`] <=="
	hmmcalibrate ${IN%.*}.hmm
	wait
}

if [ ${IN} = "clean" ] ; then
	clean
	exit
fi

if [ "$#" -ne 4 ]; then
	echo "Four parameters were expected, received $#."
	exit
fi

if [ ! -f ${IN} ]; then
	echo "Error: File ${IN} not found!"
	exit
fi

filter
mafft_align
build_hmm
# calibrate

echo "==> Done [`date`] <=="

exit
