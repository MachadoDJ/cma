# Genetic code for Cestoda

This directory contains information regarding Cestoda's mitochondrial genetic code based on translation table 9. Additional start and stop codons, from the literature or from translation table 14, are also included.

## codon.table

Codons and AA in tab separated format. Start codons are indicated by **M** and stop codons by **X**.

## start.codons

Possible start codons according to the current specialized literature. One per line. This may not necessarily code for **M** if there are found within the CDS.

## stop.codons

Possible stop codons according to the current specialized literature. One per line. Some of this may code for a AA if found within the CDS.