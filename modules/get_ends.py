def stops(frame, dir):
	handle = open("{}/stop.codons".format(dir), "r")
	possible_stop_codons = []
	for codon in handle.readlines():
		codon = codon.strip()
		if codon:
			possible_stop_codons.append(codon)
	handle.close()
	ambiguous = []
	for codon in possible_stop_codons:
		if(len(codon)==2):
			for nuc in ["A", "C", "T", "G"]:
				ambiguous += ["{}{}".format(codon, nuc)]
		elif(len(codon)==1):
			for nuc1 in ["A", "C", "T", "G"]:
				for nuc2 in ["A", "C", "T", "G"]:
					ambiguous += ["{}{}{}".format(codon, nuc1, nuc2)]
	possible_stop_codons += ambiguous
	possible_stop_codons = list(set([codon for codon in possible_stop_codons if len(codon) == 3]))
	stops = []
	for XXX in possible_stop_codons:
		stops += [index for index, codon in enumerate(frame) if codon==XXX]
	stops = sorted(stops)
	return stops

def starts(frame, dir):
	handle = open("{}/start.codons".format(dir), "r")
	possible_start_codons = []
	for codon in handle.readlines():
		codon = codon.strip()
		if codon:
			possible_start_codons.append(codon)
	handle.close()
	starts = []
	for XXX in possible_start_codons:
		starts += [index for index, codon in enumerate(frame) if codon==XXX]
	starts = sorted(starts)
	return starts
