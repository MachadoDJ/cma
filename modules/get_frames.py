def main(id, seq, n):
	frame1 = []
	frame2 = []
	frame3 = []
	i = 0
	while i <= n:
		codon1 = seq[i : i + 3]
		codon2 = seq[i + 1 : i + 4]
		codon3 = seq[i + 2 : i + 5]
		len_codon1 = len(codon1)
		len_codon2 = len(codon2)
		len_codon3 = len(codon3)
		if len_codon1 == 3:
			pass
		elif len_codon1 == 2:
			codon1 += seq[:1]
		elif len_codon1 == 1:
			codon1 += seq[:2]
		elif len_codon1 == 1:
			codon1 += seq[:2]
		if len_codon2 == 3:
			pass
		elif len_codon2 == 2:
			codon2 += seq[:1]
		elif len_codon2 == 1:
			codon2 += seq[:2]
		if len_codon3 == 3:
			pass
		elif len_codon3 == 2:
			codon3 += seq[:1]
		elif len_codon3 == 1:
			codon3 += seq[:2]
		if codon1:
			frame1 += [codon1]
		if codon2:
			frame2 += [codon2]
		if codon3:
			frame3 += [codon3]
		i += 3
	return frame1, frame2, frame3
