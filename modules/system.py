def prep_dir(outdir):
	import os, sys
	if os.path.isdir(outdir):
		sys.stderr.write("WARNING: The directory {} alread exists\n".format(outdir))
	else:
		try:
			os.makedirs(outdir)
		except:
			sys.stderr.write("ERROR: The directory {} could not be created\n".format(outdir))
			exit()
	return

def blastp(PATH_TO_AA, PATH_TO_BLAST, NPROC, outdir, verbose):
	import re, os, sys, glob, subprocess
	from operator import itemgetter
	blastp = "{}/blastp".format(PATH_TO_BLAST)
	if not os.path.exists(blastp):
		sys.stderr.write("ERROR: could not find {}\n".format(blastp))
		exit()
	else:
		for aa in sorted([aa for aa in glob.glob("{}/*.pep".format(PATH_TO_AA))]):
			prefix = re.compile("([^\.\\\/]+)\.pep").findall(aa)[0]
			if(verbose):
				sys.stdout.write("> Current database: {}\n".format(prefix))
			outfile = "{}/{}.outfmt6".format(outdir, prefix)
			query = "{}/putative_{}.pep".format(outdir, prefix)
			cmd = "blastp -query {0} -db {1} -max_target_seqs 1 -evalue 1e-5 -qcov_hsp_perc 80 -num_threads {2} -out {3} -outfmt '6 qseqid sseqid pident length mismatch gapopen qstart qend sstart send evalue bitscore qcovs'".format(query, aa, NPROC, outfile)
			subprocess.call(cmd, shell=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
			lines = [line.split() for line in open(outfile, "r").readlines()]
			for line in lines:
				for i in [12, 2, 11]:
					line[i] = float(line[i])
			lines = sorted(lines, key=itemgetter(12)) # sort by qcovs
			try:
				best_qcovs = lines[-1][12]
			except:
				if(verbose):
					sys.stdout.write("-- {} is empty (no matches for this gene)\n".format(outfile))
					pass
			else:
				lines = sorted([line for line in lines if line[12] == best_qcovs], key=itemgetter(11)) # sort by bitscore
				lines = sorted([line for line in lines if line[11]], key=itemgetter(2)) # sort by pident
				best_line = [str(i) for i in lines[-1]]
				open("{}/best_{}.outfmt6".format(outdir, prefix), "w").write("# qseqid\tsseqid\tpident\tlength\tmismatch\tgapopen\tqstart\tqend\tsstart\tsend\tevalue\tbitscore\tqcovs\n{}\n".format("\t".join(best_line)))
				if(verbose):
					sys.stdout.write("-- best match saved into {}\n".format("{}/best_{}.outfmt6".format(outdir, prefix)))
	return

def hmmer(genome, PATH_TO_HMMER, PATH_TO_RRNA, outdir):
	import glob, subprocess, re
	for hmm in glob.glob("{}/*.hmm".format(PATH_TO_RRNA)):
		prefix = re.compile("([^\\\/]+\.hmm)").findall(hmm)[0]
		hmmsearch = "{}/nhmmer".format(PATH_TO_HMMER)
		outfile = "{}/{}.out".format(outdir, prefix)
		cmd = "{} --noali --tblout {} {} {}".format(hmmsearch, outfile, hmm, genome)
		subprocess.call(cmd, shell = True)
	return

def arwen(genome, outdir, PATH_TO_ARWEN):
	import sys, subprocess
	cmd1 = "{1}/arwen -gcaltflat -j -c -fo -o {2}/tmp.j {0} -d -rp && {1}/arwen -gcaltflat -jr4 -c -fo -o {2}/tmp.jr4 {0} -d -rp".format(genome, PATH_TO_ARWEN, outdir)
	subprocess.call(cmd1, shell = True)
	handle = open("{0}/tmp.j".format(outdir), "r")
	data = handle.read()
	handle.close()
	data = data.replace(">", ">j_")
	data = data.replace(" ", "_")
	data = data.replace(" ", "__")
	handle = open("{0}/tmp.j".format(outdir), "w")
	handle.write(data)
	handle.close()
	handle = open("{0}/tmp.jr4".format(outdir), "r")
	data = handle.read()
	handle.close()
	data = data.replace(">", ">jr4_")
	data = data.replace(" ", "_")
	data = data.replace(" ", "__")
	handle = open("{0}/tmp.jr4".format(outdir), "w")
	handle.write(data)
	handle.close()
	cmd4 = "cat {0}/tmp.j {0}/tmp.jr4 > {0}/selected_trna.fasta && rm {0}/tmp.j {0}/tmp.jr4".format(outdir)
	subprocess.call(cmd4, shell = True)
	return
