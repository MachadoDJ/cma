import sys, re

def main(file):
	data = []
	handle = open(file, "r")
	try:
		for description, sequence in re.compile(">([^\n\r]+)\n([^>]+)",re.M|re.S).findall(handle.read()):
			description = description.strip()
			sequence = sequence.strip().upper()
			id = re.compile("^([^\s]+)").findall(description)[0]
			sequence = re.sub(r"[Nn\?\s]", "", sequence)
			data = [id, sequence]
		handle.close()
	except:
		sys.stderr.write("Error while parsing sequences from {}\n".format(file))
		exit()
	return data

def cds(file):
	import re, sys
	dic = {}
	handle = open(file, "r")
	for line in handle.readlines():
		line = line.strip()
		if(line):
			line = re.sub("#.*", "", line)
			if(line):
				try:
					gene, length = re.compile("(\w+)\s*=\s*(\d+)").findall(line)[0]
					dic[gene.lower()] = int(length)
				except:
					sys.stderr.write("Error: Something went wrong while trying to read data from {}.\n".format(file))
					exit()
	handle.close()
	min_orf = min([dic[i] for i in dic])
	max_orf = max([dic[i] for i in dic])
	return dic, min_orf, max_orf

def configuration(file):
	try:
		handle = open(file, "r")
	except:
		sys.stderr.write("Error trying to open {} to read\n".format(file))
		exit()
	data = handle.read().strip()
	data = re.sub("#.*", "", data)
	NPROC         = re.compile("NPROC\s*=\s*[\"\']*([^\"\']+)[\"\']*").findall(data)[0]
	PATH_TO_AA    = re.compile("PATH_TO_AA\s*=\s*[\"\']*([^\"\']+)[\"\']*").findall(data)[0]
	PATH_TO_RRNA  = re.compile("PATH_TO_RRNA\s*=\s*[\"\']*([^\"\']+)[\"\']*").findall(data)[0]
	PATH_TO_TRNA  = re.compile("PATH_TO_TRNA\s*=\s*[\"\']*([^\"\']+)[\"\']*").findall(data)[0]
	PATH_TO_PY    = re.compile("PATH_TO_PY\s*=\s*[\"\']*([^\"\']+)[\"\']*").findall(data)[0]
	PATH_TO_ARWEN = re.compile("PATH_TO_ARWEN\s*=\s*[\"\']*([^\"\']+)[\"\']*").findall(data)[0]
	PATH_TO_BLAST = re.compile("PATH_TO_BLAST\s*=\s*[\"\']*([^\"\']+)[\"\']*").findall(data)[0]
	PATH_TO_HMMER = re.compile("PATH_TO_HMMER\s*=\s*[\"\']*([^\"\']+)[\"\']*").findall(data)[0]
	PATH_TO_MAFFT = re.compile("PATH_TO_MAFFT\s*=\s*[\"\']*([^\"\']+)[\"\']*").findall(data)[0]
	PATH_TO_CODE  = re.compile("PATH_TO_CODE\s*=\s*[\"\']*([^\"\']+)[\"\']*").findall(data)[0]
	handle.close()
	return NPROC, PATH_TO_AA, PATH_TO_RRNA, PATH_TO_TRNA, PATH_TO_PY, PATH_TO_ARWEN, PATH_TO_BLAST, PATH_TO_HMMER, PATH_TO_MAFFT, PATH_TO_CODE

def rrna(seq, dir):
	import glob, re
	from operator import itemgetter
	outfile = open("{}/selected_rrna.fasta".format(dir), "w")
	for file in glob.glob("{}/*.hmm.out".format(dir)):
		gene   = re.compile("([^\\\/]+)\.hmm\.out").findall(file)[0]
		handle = open(file, "r")
		lines  = [line.strip() for line in handle.readlines() if line.strip()[0] != "#"]
		handle.close()
		clip = []
		for line in lines:
			line = re.compile("([^\s]+)").findall(line)
			start  = int(line[8])
			end    = int(line[9])
			strand = line[11]
			e      = float(line[12])
			score  = float(line[13])
			bias  = float(line[14])
			clip += [[e, score, bias, start, end, strand]]
		clip = sorted(clip, key=itemgetter(0))
		clip = sorted(clip, key=itemgetter(1))
		clip = sorted(clip, key=itemgetter(2))
		e, score, bias, start, end, strand = clip[-1]
		start, end = sorted([int(i) for i in [start, end]])
		score, e   = [float(i) for i in [score, e]]
		rrna       = seq[start + 1 : end]
		if strand != "+":
			output = ">{} c[{}:{}] score={} E={}\n{}\n".format(gene, start, end, score, e, rrna)
		else:
			output = ">{} [{}:{}] score={} E={}\n{}\n".format(gene, start, end, score, e, rrna)
		sys.stdout.write(output)
		outfile.write(output)
	outfile.close()
	return

def trna(dir, PATH_TO_BLAST, PATH_TO_TRNA, NPROC):
	import subprocess, re, glob
	from Bio import SeqIO
	dic = {}
	selected = {}
	for record in SeqIO.parse(open("{}/selected_trna.fasta".format(dir), "r"), "fasta"):
		des = record.description
		seq = str(record.seq)
		dic[seq] = des
	for seq in dic:
		des = dic[seq]
		selected[des] = seq
	handle = open("{}/selected_trna.fasta".format(dir), "w")
	for des in selected:
		handle.write(">{}\n{}\n".format(des, selected[des]))
	handle.close()
	for db in glob.glob("{}/*.fasta".format(PATH_TO_TRNA)):
		gene = re.compile("([^\\\/]+)\.fasta").findall(db)[0]
		cmd = "{4}/blastn -evalue 0.001 -query {0}/selected_trna.fasta -outfmt 6 -num_threads {1} -max_target_seqs 1 -db {2} -out {0}/trna_{3}.outfmt6".format(dir, NPROC, db, gene, PATH_TO_BLAST)
		subprocess.call(cmd, shell = True)
	return
