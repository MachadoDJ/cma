# This module will export the first temporary fasta file containing all
# putative ORFs which length is bigger than the specified threshold.

def main(working_frame, putative_orfs, count, outfile):
	dna = ""
	for x, y in putative_orfs:
		id = ">orf{}".format(count)
		seq = "".join(working_frame[x : y + 1])
		dna += "{}\n{}\n".format(id, seq)
		count += 1
	handle = open(outfile, "a")
	handle.write(dna)
	handle.close()
	return count
