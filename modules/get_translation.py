# This module will translate all putative ORFs. During this process,
# ORFs with potential stop codons in the middle of the sequence (as
# specified in the codon table by the letter X) will be removed from the
# database.

import re

def main(infile, dir, outdir, cds_lengths, p):
	dic = {}
	pep_dic = {}
	dna_dic = {}
	for cds in cds_lengths:
		pep_dic[cds] = ""
		dna_dic[cds] = ""
	prefix = re.compile("^([^\.]+)").findall(infile)[0]
	outfile = "{}.pep".format(prefix)
	handle = open("{}/codon.table".format(dir), "r")
	# Read the codon table
	for line in handle.readlines():
		line = line.strip()
		if line:
			line = line.upper()
			codon, aa = line.split("\t")
			dic[codon] = aa
	handle.close()
	# Get other potential stop codons from a different file
	final_codons = get_stop_codons(dir)
	handle = open(infile, "r")
	# Read all putative ORFs
	for id, seq in re.compile(">([^\n\r]+)\n([^>]+)",re.M|re.S).findall(handle.read()):
		id = id.strip()
		seq = seq.strip()
		frame1 = get_frame1(seq)
		# Below, I trimmed putative stop codons
		if(not frame1[-1] in final_codons):
			if(frame1[-1][:2] in final_codons):
				frame1[-1] = frame1[-1][:2]
			else:
				frame1[-1] = frame1[-1][:1]
		# The translation happens here
		try:
			translated_seq = "M" + "".join([dic[codon] for codon in frame1[:-1]][1:])
		except:
			pass
		# The sequence will be discaded if a stop codon is found within it
		if(not "X" in translated_seq[1:-1]):
			dna_seq = "".join(frame1)
			dna_len = len(dna_seq)
			for cds in cds_lengths:
				min_val = cds_lengths[cds] * (1 - p)
				max_val = cds_lengths[cds] * (1 + p)
				if(dna_len >= min_val and dna_len <= max_val):
					dna_dic[cds] += ">{}\n{}\n".format(id, dna_seq)
					pep_dic[cds] += ">{}\n{}\n".format(id, translated_seq)
	handle.close()
	for cds in dna_dic:
		handle = open("{}/putative_{}.fas".format(outdir, cds), "w")
		handle.write(dna_dic[cds])
		handle.close()
		handle = open("{}/putative_{}.pep".format(outdir, cds), "w")
		handle.write(pep_dic[cds])
		handle.close()
	return

# This function will parse all the putative stop codons
def get_stop_codons(dir):
	handle = open("{}/stop.codons".format(dir), "r")
	final_codons = []
	for codon in handle.readlines():
		codon = codon.strip()
		if codon:
			final_codons.append(codon)
	handle.close()
	return final_codons

# This function will break the sequence into a list of codons
def get_frame1(seq):
	frame1 = []
	i = 0
	while i <= len(seq):
		codon1 = seq[i : i + 3]
		if len(codon1) == 3:
			frame1 += [codon1]
		i += 3
	return frame1
