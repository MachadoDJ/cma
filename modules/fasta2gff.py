def parse_seqid(file):
	import re
	handle = open(file, "r")
	data = handle.read()
	handle.close()
	seqid = re.compile(">\s*([^\s\n ]+)").findall(data)[0]
	return seqid

def parse_fasta(f):
	import re
	dic = {}
	handle = open(f, "r")
	data = handle.read()
	handle.close()
	data = [i.strip().split() for i in re.compile(">(.+)").findall(data)]
	for item in data:
		gene, position, score1, score2 = item
		if position[0] == "c":
			frame = "-"
		else:
			frame = "+"
		start, end = re.compile("(\d+)").findall(position)
		score1 = re.sub(".+?=", "", score1)
		# score2 = re.sub(".+?=", "", score2)
		dic[gene] = [start, end, score1, score2, frame]
	return dic

def concat_dic(a, b, c, seqid):
	import sys
	from operator import itemgetter
	list = [a, b, c]
	footle = open("output.gff", "w")
	header = "##gff-version 3"
	output = []
	for i in list:
		for k, v in i.items():
			line = ""
			line += "{}\t".format(seqid)
			if i == a:
				line += "blastp\tCDS"
			elif i == b:
				line += "hmmer\trRNA"
			elif i == c:
				line += "arwen/blastn\ttRNA"
			else:
				sys.stdwrite.error("ERROR: Something went wrong while writting the gff output file.\n")
				return
			line += "\t{0}\t{1}\t{2}\t{3}\t0\tName={4};{5}\n".format(v[0], v[1], v[2], v[4], k, v[3])
			line = line.split("\t")
			line[3] = int(line[3])
			output += [line]
	output = sorted(output, key=itemgetter(3))
	footle = open("output.gff", "w")
	for line in output:
		line[3] = str(line[3])
		footle.write("{}".format("\t".join(line)))
	footle.close()
	return
