# This module will extract all putative ORFs longer than the specified threshold.

def putative(frame, starts, stops, minimum, maximum, dir):
	stop_codons = []
	# Read the codon table
	handle = open("{}/codon.table".format(dir), "r")
	for line in handle.readlines():
		line = line.strip()
		if line:
			line = line.upper()
			codon, aa = line.split("\t")
			if(aa == "X"):
				stop_codons += [codon]
	handle.close()
	stop_codons = set(stop_codons)
	putative_orfs = []
	for i in starts:
		for j in [j for j in [j for j in stops if (j - i) * 3 >= minimum] if (j - i) * 3 <= maximum]:
			seq = set(frame[i+1:j])
			if(stop_codons & seq):
				pass
			else:
				putative_orfs += [[i, j]]
	return putative_orfs
