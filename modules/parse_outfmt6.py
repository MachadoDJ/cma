import glob, sys, re, operator
from Bio import SeqIO

# Define functions

def main(dir, verbose):
	hits = {}
	complete = {}
	if(verbose):
		sys.stdout.write("Gene\tID\te-value\tbit-score\tqcovs\tpident\tlength\n")
	for filename in glob.glob("{}/best_*.outfmt6".format(dir)):
		handle = open(filename, "r")
		for line in handle.readlines():
			line = re.sub("#.*", "", line)
			line = line.strip()
			if(line):
				qseqid, sseqid, pident, length, mismatch, gapopen, qstart, qend, sstart, send, evalue, bitscore, qcovs = line.split("\t")
				evalue = float(evalue)
				bitscore = float(bitscore)
				qstart = int(qstart)
				qend = int(qend)
				gene_name = re.compile("^([^\_]+)\_.+").findall(sseqid)[0]
				complete["{}_{}".format(gene_name, qseqid)] = [evalue, bitscore]
				sys.stdout.write("{}\t{}\t{}\t{}\t{}\t{}\t{}\n".format(gene_name, qseqid, evalue, bitscore, qcovs, pident, int(send) - int(sstart)))
				if not gene_name in hits:
					hits[gene_name] = [evalue, bitscore, qseqid]
				elif evalue <= hits[gene_name][0] and bitscore > hits[gene_name][0]:
					hits[gene_name] = [evalue, bitscore, qseqid]
		handle.close()
	selection = report(hits, verbose)
	parse_fasta(selection, hits, dir, complete)
	return

def report(hits, verbose):
	if(verbose):
		sys.stdout.write("# Final selection:\nGene\tID\te-value\tbit-score\n")
	genes = []
	selection = {}
	for gene_name in hits:
		evalue, bitscore, qseqid = hits[gene_name]
		if(verbose):
			sys.stdout.write("{}\t{}\t{}\t{}\n".format(gene_name, qseqid, evalue, bitscore))
		selection[qseqid] = gene_name
		if not gene_name in genes:
			genes.append(gene_name)
	if(verbose):
		sys.stdout.write("# No. of CDS found: {}\n# No. of ORFs selected: {}\n".format(len(genes), len(selection)))
	return selection

def parse_fasta(selection, hits, dir, complete):
	fasta_out = ""
	for record in SeqIO.parse("{}/putative_orfs.fasta".format(dir), "fasta"):
		if record.id in selection:
			gene_name = selection[record.id]
			key = "{}_{}".format(gene_name, record.id)
			evalue, bitscore = complete[key]
			fasta_out += ">{} evalue={} bitscore={}\n{}\n".format(gene_name, evalue, bitscore, record.seq)
	handle = open("{}/selected_orfs.fasta".format(dir), "w")
	handle.write(fasta_out)
	handle.close()
	return

def find_range(genome, dir):
	new = {}
	handle = open("{}/selected_orfs.fasta".format(dir), "r")
	for id, seq in re.compile("(>[^\n]+)\n([^>]+)", re.M|re.S).findall(handle.read()):
		seq = re.sub(r"[\s\n]", "", seq)
		i = genome.find(seq)
		j = i + len(seq)
		a, b = re.compile("(>[^\s]+) (.+)").findall(id.strip())[0]
		id = "{} [{}:{}] {}".format(a, i + 1, j, b)
		new[id] = seq
	handle.close()
	handle = open("{}/selected_orfs.fasta".format(dir), "w")
	for id in new:
		handle.write("{}\n{}\n".format(id, new[id]))
	handle.close()
	return

def trna(dir):
	annotations = {}
	stats = {}
	for tab in glob.glob("{}/trna_*.outfmt6".format(dir)):
		handle = open(tab, "r")
		all = {}
		for line in handle.readlines():
			line = line.strip()
			if line:
				try:
					qseqid, sseqid, pident, length, mismatch, gapopen, qstart, qend, sstart, send, evalue, bitscore = line.split()
				except:
					sys.stderr.write("Error while reading {}.\n".format(tab))
					exit()
				else:
					bitscore = float(bitscore)
					evalue = float(evalue)
					gene = re.compile("([^\\\/_]+)\..*").findall(tab)[0]
					gene = "mtRNA-{}{}".format(gene[0].upper(), gene[1:].lower())
					start, end = [int(i) for i in re.compile("\[(\d+),(\d+)\]").findall(qseqid)[0]]
					stats["{}_{}_{}".format(gene, start, end)] = [evalue, bitscore]
					all[qseqid] = [start, end, evalue, bitscore]
		handle.close()
		ids = [qseqid for qseqid in all]
		sel = {}
		if len(ids) == 1:
			for qseqid in all:
				sel[qseqid] = [all[qseqid][2], -1.0 * all[qseqid][2]]
		else:
			while ids:
				current = ids[0]
				s1 , e1, ev1, sc1 = all[current]
				s1, e1 = sorted([s1, e1])
				ids = ids[1:]
				if ids:
					for i in range(0, len(ids)):
						next = ids[i]
						s2, e2, ev2, sc2 = all[next]
						s2, e2 = sorted([s2, e2])
						if (s1 <= s2 and e1 >= s2) or (s2 <= s1 and e2 >= s1) and (ev1 >= ev2 and sc1 <= sc2):
							current = "reject"
							break
				if current != "reject":
					sel[current] = [ev1, -1.0 * sc1]
		for key in [key for key, value in sorted(sel.items(), key=operator.itemgetter(1))][:2]:
			annotations[key] = gene
	report_trna(annotations, stats, dir)
	return annotations

def report_trna(annotations, stats, dir):
	new = {}
	for record in SeqIO.parse(open("{}/selected_trna.fasta".format(dir), "r"), "fasta"):
		if record.id in annotations:
			seq = str(record.seq)
			interval = re.compile("([c]*\[\d+,\d+\])").findall(record.id)[0]
			interval = re.sub(",", ":", interval)
			start, end = re.compile("(\d+):(\d+)").findall(interval)[0]
			gene = annotations[record.id]
			key = "{}_{}_{}".format(gene, start, end)
			evalue = stats[key][0]
			bitscore = stats[key][1]
			name = "{} {} evalue={} bitscore={}".format(gene, interval, evalue, bitscore)
			new[name] = seq
			sys.stdout.write(">{}\n{}\n".format(name, seq))
	handle = open("{}/selected_trna.fasta".format(dir), "w")
	for name in new:
		handle.write(">{}\n{}\n".format(name, new[name]))
	handle.close()

	return
