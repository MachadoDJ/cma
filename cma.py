#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# cma.py
# Cestode mtDNA annotator (CDS, rRNA, and tRNA).

####
## Import external modules
####

import argparse, re, sys, os, glob

try:
	CMA_HOME=os.environ.get('CMA_HOME')
except:
	sys.stderr.write("ERROR: Could not find the environmental variable CMA_HOME. Create it by typing 'export CMA_HOME=/full/path/to/cma/directory'\n")
	exit()

####
## Internal modules
####

sys.path.append("{}/modules/".format(CMA_HOME)) # !!! Be sure to export CMA_HOME as a sytem's variable !!!
import get_data
import get_frames
import get_ends
import get_orfs
import report_orfs
import get_translation
import system
import parse_outfmt6
import fasta2gff

####
## Set arguments
####

parser=argparse.ArgumentParser()
parser.add_argument("-g", "--genome", help = "One mitogenome in FASTA format", type = str, required = True)
parser.add_argument("-c", "--configuration", help = "Configuration file (default = 'configuration.txt')", type = str, default = "configuration.txt", required = False)
parser.add_argument("-d", "--dir", help = "Directory into which output files will be saved (default = 'output')", type = str, default = "output", required = False)
parser.add_argument("-l", "--lengths", help = "Expected length for each protein coding gene (default = 'gene_lengths.txt').", default = "gene_lengths.txt", type = str, required = False)
parser.add_argument("-p", "--percent", help = "Percent (in decimals) of variation allowed over each expected protein coding gene length (default = 0.2).", default = 0.2, type = float, required = False)
parser.add_argument("-v", "--verbose", help = "Increases output verbosity", action = "store_true", default = False, required = False)
args=parser.parse_args()


####
## Get configuration
####

if(args.verbose):
	sys.stdout.write("## CONFIGURATION ##\n")
NPROC, PATH_TO_AA, PATH_TO_RRNA, PATH_TO_TRNA, PATH_TO_PY, PATH_TO_ARWEN, PATH_TO_BLAST, PATH_TO_HMMER, PATH_TO_MAFFT, PATH_TO_CODE = get_data.configuration(args.configuration)
if(args.verbose):
	sys.stdout.write("NPROC = {}\nPATH_TO_AA = {}\nPATH_TO_RRNA = {}\nPATH_TO_TRNA = {}\nPATH_TO_PY = {}\nPATH_TO_ARWEN = {}\nPATH_TO_BLAST = {}\nPATH_TO_HMMER = {}\nPATH_TO_MAFFT = {}\nPATH_TO_CODE = {}\n".format(NPROC, PATH_TO_AA, PATH_TO_RRNA, PATH_TO_TRNA, PATH_TO_PY, PATH_TO_ARWEN, PATH_TO_BLAST, PATH_TO_HMMER, PATH_TO_MAFFT, PATH_TO_CODE))

####
## Get expected lengths
####

if(args.verbose):
	sys.stdout.write("## EXPECTED CDS LENGTH ##\n")
cds_lengths, min_orf, max_orf = get_data.cds(args.lengths)
min_orf = int(round(min_orf * (1-args.percent)))
max_orf = int(round(max_orf * (1+args.percent)))
if(args.verbose):
	for cds in sorted(cds_lengths):
		sys.stdout.write("{} = {} [{:.2f} : {:.2f}]\n".format(cds, int(cds_lengths[cds]), cds_lengths[cds]*(1-args.percent), cds_lengths[cds]*(1+args.percent)))

####
## Prepare output directory
####

if(args.verbose):
	sys.stdout.write("## OUTPUT DIR ##\n")
system.prep_dir(args.dir)
if(args.verbose):
	sys.stdout.write("> Files will be saved into {}/\n".format(args.dir))

####
## Get putative ORFs (forward only)
####

if(args.verbose):
	sys.stdout.write("## PREDICTING PUTATIVE ORFS ##\nAttention: forward only.\n")
outfile = "{}/putative_orfs.fasta".format(args.dir)
if not os.path.exists(outfile):
	handle = open(outfile, "w")
	handle.close()
	data = get_data.main(args.genome)
	m = len(data) / 2
	if m > 1:
		sys.stderr.write("¡ This pipeline is for one sequence at a time, {} given !\n".format(m))
		exit()
	else:
		count = 0
		id, seq = data
		n = len(seq)
		frame1, frame2, frame3 = get_frames.main(id, seq, n)
		for working_frame in [frame1, frame2, frame3]:
			if(args.verbose):
				sys.stdout.write("-- new frame\n")
			if(args.verbose):
				sys.stdout.write("-- collecting all possible stop positions\n")
			stops = get_ends.stops(working_frame, PATH_TO_CODE)
			if(args.verbose):
				sys.stdout.write("-- collecting all possible start positions\n")
			starts = get_ends.starts(working_frame, PATH_TO_CODE)
			if(args.verbose):
				sys.stdout.write("-- collecting all possible ORFs\n")
			putative_orfs = get_orfs.putative(working_frame, starts, stops, min_orf, max_orf, PATH_TO_CODE)
			if(args.verbose):
				sys.stdout.write("-- writing temporary files\n")
			count = report_orfs.main(working_frame, putative_orfs, count, outfile)
	if(args.verbose):
		sys.stdout.write("-- translation and filtering\n")
	get_translation.main(outfile, PATH_TO_CODE, args.dir, cds_lengths, args.percent)
	if(args.verbose):
		sys.stdout.write("> Putative ORFs (DNA and AA) written into {} (see putative_orfs.fasta* and putative_orfs.pep*).\n".format(args.dir))
else:
	sys.stderr.write("¡ File {} already exists !\n".format(outfile))
	exit()

####
## Run BlastP
####

if(args.verbose):
	sys.stdout.write("## RUNNING BLASTP ##\n")
system.blastp(PATH_TO_AA, PATH_TO_BLAST, NPROC, args.dir, args.verbose)
if(args.verbose):
	sys.stdout.write("> Output tables in format 6 (outfmt6) were written into {}/.\n-- Columns: qseqid, sseqid, pident, length, mismatch, gapopen, qstart, qend, sstart, send, evalue, bitscore, and qcovs.\n-- Selection: sort by qcovs first, then bitscore, and finally pident.\n".format(args.dir))

####
## Parse outfmt6 files
####

if(args.verbose):
	sys.stdout.write("## PARSE HIT TABLES - OUTFMT6 ##\n")
parse_outfmt6.main(args.dir, args.verbose)
parse_outfmt6.find_range(seq, args.dir)
if(args.verbose):
	sys.stdout.write("> Putative CDS sequences were saved in {}/selected_orfs.fasta.\n".format(args.dir))

####
## mRNA prediction
####

if(args.verbose):
	sys.stdout.write("## RNA ANNOTATION WITH HMMER ##\n")
system.hmmer(args.genome, PATH_TO_HMMER, PATH_TO_RRNA, args.dir)
get_data.rrna(seq, args.dir)
if(args.verbose):
	sys.stdout.write("> Putative rRNA sequences were written in {}/selected_rrna.fasta.\n".format(args.dir))

####
## tRNA prediction
####

if(args.verbose):
	sys.stdout.write("## RNA ANNOTATION WITH ARWEN ##\n")
system.arwen(args.genome, args.dir, PATH_TO_ARWEN)
get_data.trna(args.dir, PATH_TO_BLAST, PATH_TO_TRNA, NPROC)
parse_outfmt6.trna(args.dir)
if(args.verbose):
	sys.stdout.write("> Putative tRNA sequences were written in {}/selected_trna.fasta.\n".format(args.dir))

####
## Produce annotation in GFF3 format
####

genome = "{}".format(args.genome)
orfs =   "{}/selected_orfs.fasta".format(args.dir)
rrna =   "{}/selected_rrna.fasta".format(args.dir)
trna =   "{}/selected_trna.fasta".format(args.dir)
seqid    = fasta2gff.parse_seqid(genome)
dic_orfs = fasta2gff.parse_fasta(orfs)
dic_rrna = fasta2gff.parse_fasta(rrna)
dic_trna = fasta2gff.parse_fasta(trna)
fasta2gff.concat_dic(dic_orfs, dic_rrna, dic_trna, seqid)

####
## Quit
####

exit()
