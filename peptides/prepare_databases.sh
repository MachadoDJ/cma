#!/bin/bash
set -o errexit

function clean {
	for i in *.pep.* ; do
		if [ -f ${i} ] ; then
			rm ${i}
		fi
	done
}

function main {
	for i in *.pep ; do
		makeblastdb -in ${i} -input_type fasta -dbtype prot -parse_seqids
	done
}

clean
main

exit
