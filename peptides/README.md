# Source

Pepetide sequences herein were combined with data from complete mitogenomes
downloaded from NCBI's GenBank.

# Date of last modification

March 31st, 2019.

# How to prepare the databases

Databases are prepared with `makeblastdb`. File names must remain the same. Every sequence of a file must start with the prefix of the filename followed by an underscore. See `prepare_databases.sh` if you need help.
