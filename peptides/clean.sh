#!/bin/bash
set -o errexit

function clean {
	for i in *.pep.* ; do
		if [ -f ${i} ] ; then
			rm ${i}
		fi
	done
}

clean

exit
