# WHAT IS THIS

The following is a complete pipeline to assemble complete mitogenomes of cestodes (the Cestode Mitogenome Annotator—CMA).

*Input* data is a single file containing one mitochondrial genome (mitogenome) in FASTA format.

*Output* includes, minimally, the DNA sequences of putative coding, rRNA, and tRNA genes.

# OBJECTIVE

If the assembly is successful, we hope to provide the DNA sequences of each CD, rRNA, and tRNA, as well as some statistics regarding annotation quality.

# DEPENDENCIES

This pipeline was designed for Linux and tested on a Ubuntu v18.04.2 LTS (Bionic Beaver). Minor tweaks might be necessary to make it work on another environment, such as editing the sed command on modules/system.py.

Below there is a list of dependencies and their version. Versions listed herein were tested, and different versions may not work as intended.

+ [ARWEN v1.2](http://130.235.244.92/ARWEN/)
+ [blast v2.4.0](https://blast.ncbi.nlm.nih.gov/Blast.cgi?CMD=Web&PAGE_TYPE=BlastDocs&DOC_TYPE=Download)
+ [HMMER v2.3.2 (stable)](http://hmmer.org/download.html)
+ [MAFFT v7.310 (2017/Mar/17)](https://mafft.cbrc.jp/alignment/software/)
+ [Python v3.6.5](https://www.python.org/downloads/) (with [Numpy](http://www.numpy.org/) and [Biopython](https://biopython.org/))

Make sure that the configuration file (usually configuration.txt) has the correct path to all dependencies, including the directories containing the required databases.

# BEFORE EXECUTING

Before executing any of the scripts herein:

1. Check the shebang (first line, starting with #!) of every script and see if it matches your system's specifications.
2. Create an environmental variable named `CMA_HOME` pointing to the CMA's directory.
3. Prepare the peptide, rRNA, and tRNA databases using the scripts that come in each of the corresponding directories.

To see details on executing Python scripts, use the argument `--help` or `-h`.

Bash scripts are commented on. Check their comments for details on what they do, their dependencies, and how to use them.

# USAGE

The CMA pipeline can be executed as follows:

```bash
$ python3 cma.py -c configuration.txt -l gene_lengths.txt -p 0.25 -g test.fasta -v
```

## Before running CMA

1. Be sure you have all the dependencies installed (as listed above)
2. Be sure to have prepared all the peptide, rRNA, and tRNA libraries (using scripts inside the corresponding directories)
3. Be sure to edit the configuration file accordingly (see instructions inside the template configuration file)

## Expected outputs

Animal mitochondrion DNA is approximately 16 kb in size. The size of the cestode mitochondrion genome varies depending on the specific species, but it typically falls within a range of 13,369 bp to 13,795 bp.

With few exceptions, all animal mitochondrial genomes contain two rRNAs, 13 for proteins and 22 for tRNAs (37 genes total). However, the cestode mitogenome typically lacks one protein-coding gene for ATP8.

Therefore, the expected output includes an output directory with sequences organized by type (including `selected_orfs.fasta`, 	`selected_rrna.fasta`, and `selected_trna.fasta`) and a GFF file with the expected 36 genes annotated.

# Web app

A web-based version of this application (called CMA-Web) was available [here](http://lhe.ib.usp.br/cma) but is no longer being maintained. An alternative, more stable web-server is under construction.