'''
This is a Python3 script that uses BioPython. It reads a complete mitochondrion genome sequence in GenBank format and produces three output FASTA files with CDS, rRNA, and tRNA nucleotide sequences.
'''

from Bio import SeqIO

def extract_sequences(genbank_file):
    cds_sequences = []
    rrna_sequences = []
    trna_sequences = []

    for record in SeqIO.parse(genbank_file, "genbank"):
        for feature in record.features:
            product_name = feature.qualifiers.get("product", ["Unknown"])[0]

            if feature.type == "CDS":
                cds_sequences.append((product_name, feature.extract(record).seq))
            elif feature.type == "rRNA":
                rrna_sequences.append((product_name, feature.extract(record).seq))
            elif feature.type == "tRNA":
                trna_sequences.append((product_name, feature.extract(record).seq))

    return cds_sequences, rrna_sequences, trna_sequences

def write_fasta(output_file, sequences):
    with open(output_file, "w") as file:
        for i, (product_name, sequence) in enumerate(sequences, start=1):
            file.write(f">{product_name} sequence_{i}\n{sequence}\n")

def main(genbank_file):
    cds_sequences, rrna_sequences, trna_sequences = extract_sequences(genbank_file)
    write_fasta("cds.fna", cds_sequences)
    write_fasta("rrna.fna", rrna_sequences)
    write_fasta("trna.fna", trna_sequences)

if __name__ == "__main__":
    genbank_file = "test.gb"
    main(genbank_file)
