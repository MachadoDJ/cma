# Contents

This directory contains auxiliary scripts for functions associated with CMA, such as extracting genes from GenBank files or selecting only unique entries from multicast files.

# Scripts

## cma_extractor.py

Extract genes from GenBank files containing one or the complete mitochondrion genome sequences.

## cma_select_unique.py

Reads all entries from a multi-FASTA file and returns only the unique sequences.

## cma_extract.nucleotides.py

This is a Python3 script that uses BioPython. It reads a complete mitochondrion genome sequence in GenBank format and produces three output FASTA files with CDS, rRNA, and tRNA nucleotide sequences.