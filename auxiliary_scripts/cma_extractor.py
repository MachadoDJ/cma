#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# cma_extractor.py

####
## Import external modules
####

import argparse, re, sys
from Bio import SeqIO

####
## Set arguments
####

parser=argparse.ArgumentParser()
parser.add_argument("-g", "--genome", help = "One genebank file containing multiple mitogenomes in genbank format (default = 'sequence.bgb')", type = str, default = "sequence.gb", required = False)
args=parser.parse_args()


def get_data():
	tRNAwhitelist = ["tRNA-Ala", "tRNA-Arg", "tRNA-Asn", "tRNA-Asp", "tRNA-Cys", "tRNA-Gln", "tRNA-Glu", "tRNA-Gly", "tRNA-His", "tRNA-Ile", "tRNA-Leu1" , "tRNA-Leu2" , "tRNA-Lys", "tRNA-Met", "tRNA-Phe", "tRNA-Pro", "tRNA-Ser1" , "tRNA-Ser2" , "tRNA-Thr", "tRNA-Trp", "tRNA-Tyr", "tRNA-Val"]
	tRNAdic={}
	for product in tRNAwhitelist:
		tRNAdic[product] = ""
	rRNAwhitelist = ["l-rRNA", "s-rRNA"]
	rRNAdic={}
	for product in rRNAwhitelist:
		rRNAdic[product] = ""
	CDSwhitelist = ["atp6", "cytb", "cox1", "cox2", "cox3", "nd1", "nd2", "nd3", "nd4", "nd4l", "nd5", "nd6"]
	CDSdic={}
	for gene in CDSwhitelist:
		CDSdic[gene] = ""
	for record in SeqIO.parse(args.genome, "genbank"):
		count_leu = 0
		count_ser = 0
		for feature in record.features:
			if(feature.type == "tRNA"):
				try:
					product = feature.qualifiers["product"][0]
				except:
					continue
				if (product == "tRNA-Leu"):
					count_leu += 1
					product += "{}".format(count_leu)
				elif (product == "tRNA-Ser"):
					count_ser += 1
					product += "{}".format(count_ser)
				if(product in tRNAdic):
					tRNAdic[product] += ">{}_{}\n{}\n".format(product, record.id, feature.extract(record.seq))
			elif(feature.type == "rRNA"):
				try:
					product = feature.qualifiers["product"][0]
				except:
					continue
				if(product in rRNAdic):
					rRNAdic[product] += ">{}_{}\n{}\n".format(product, record.id, feature.extract(record.seq))
			elif(feature.type == "CDS"):
				try:
					gene = feature.qualifiers["gene"][0].lower()
				except:
					continue
				if(("n" in gene) and ("d" in gene)):
					if("1" in gene):
						gene = "nd1"
					elif("2" in gene):
						gene = "nd2"
					elif("3" in gene):
						gene = "nd3"
					elif("5" in gene):
						gene = "nd5"
					elif("6" in gene):
						gene = "nd6"
					elif("l" in gene):
						gene = "nd4l"
					elif("4" in gene):
						gene = "nd4"
				elif("c" in gene):
					if(("1" in gene) or ("coi" == gene)):
						gene = "cox1"
					elif(("2" in gene) or ("coxii" == gene)):
						gene = "cox2"
					elif(("3" in gene) or ("coiii" == gene)):
						gene = "cox3"
					elif(("y" in gene) or ("t" in gene)):
						gene = "cytb"
				elif(("a" in gene) and ("6" in gene)):
					gene = "atp6"
				if(gene in CDSdic):
					CDSdic[gene] += ">{}_{}\n{}\n".format(gene, record.id, feature.qualifiers["translation"][0])
	return CDSdic, rRNAdic, tRNAdic

def report_data(CDSdic, rRNAdic, tRNAdic):
	for gene in CDSdic:
		handle = open("{}.pep".format(gene), "w")
		handle.write(CDSdic[gene])
		handle.close()
	for product in rRNAdic:
		handle = open("{}.fasta".format(product), "w")
		handle.write(rRNAdic[product])
		handle.close()
	for product in tRNAdic:
		handle = open("{}.fasta".format(product), "w")
		handle.write(tRNAdic[product])
		handle.close()
	return

CDSdic, rRNAdic, tRNAdic = get_data()
report_data(CDSdic, rRNAdic, tRNAdic)

exit()
