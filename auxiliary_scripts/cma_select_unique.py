#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# cma_select_unique.py

####
## Import external modules
####

import argparse, re, sys
from Bio import SeqIO

####
## Set arguments
####

parser=argparse.ArgumentParser()
parser.add_argument("-f", "--fasta", help = "Multifasta file", type = str, required = True)
args=parser.parse_args()


def select_unique():
	dic = {}
	for record in SeqIO.parse(args.fasta, "fasta"):
		name = record.description
		dic[record.seq] = record.description
	return dic

def report(dic):
	handle = open("{}.mod".format(args.fasta), "w")
	for seq in dic:
		handle.write(">{}\n{}\n".format(dic[seq], seq))
	handle.close()
	return

dic = select_unique()
report(dic)

exit()
