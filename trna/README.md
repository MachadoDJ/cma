# Lasted modification

March 31st., 2019.

# Source of data

## RNA Central

* Source https://rnacentral.org
* Keywords: rna_type:"tRNA" AND Cestoda
* Additional keywords (gene names): tRNA-Ala, tRNA-Arg, tRNA-Asn,
	tRNA-Asp, tRNA-Cys, tRNA-Gln, tRNA-Glu, tRNA-Gly, tRNA-His, tRNA-Ile,
	tRNA-Leu1*, tRNA-Leu2*, tRNA-Lys, tRNA-Met, tRNA-Phe, tRNA-Pro,
	tRNA-Ser1*, tRNA-Ser2*, tRNA-Thr, tRNA-Trp, tRNA-Tyr, and tRNA-Val
* Repeated tRNA: Leu and Ser (they were deleted from this database)

## NCBI

tRNA sequences were extracted from RefSeq complete mitochondrion genomes of Cestoda.

## Notes

Databases have to be prepared with `makeblastdb`. See `prepare_databases.sh` for additional help.
