#!/bin/bash
set -o errexit

function clean {
	for i in *.fasta.* ; do
		if [ -f ${i} ] ; then
			rm ${i}
		fi
	done
}

function main {
	for i in *.fasta ; do
		makeblastdb -in ${i} -input_type fasta -dbtype nucl -parse_seqids
	done
}

clean
main

exit
