#!/bin/bash
set -o errexit

function clean {
	for i in *.fasta.* ; do
		if [ -f ${i} ] ; then
			rm ${i}
		fi
	done
}

clean

exit
